<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# Oniro Project Documentation Repository

[![Documentation Status](https://readthedocs.org/projects/oniroproject/badge/?version=latest)](https://docs.oniroproject.org/en/latest/)

This repository provides the overarching documentation of the Oniro
Project. It does that by aggregating multiple `sphinx` projects part of
the Oniro Project ecosystem and consolidating all these documentation trees as part of
one, project-wide, `sphinx` project.

## Read The Docs

As part of the Oniro Project infrastructure, this documentation is pushed to a
Read The Docs project. You can inspect the `latest` version
[online](https://docs.oniroproject.org/en/latest/) or download it as
[PDF](https://docs.oniroproject.org/_/downloads/en/latest/pdf/).

## Building the Docs

As per the current implementation for satisfying aggregation of multiple
`sphinx` projects, the repository takes advantage of symlinks which assume a
workspace set up from [the oniro repository](https://gitlab.eclipse.org/eclipse/oniro-core/oniro).
Once that is initialized and synced, one can build/validate the documentation
by simply running:

```
make
```

For the above command to succeed, ensure all the [prerequisites](https://docs.oniroproject.org/en/latest/building-project-documentation.html) are satisfied.

## Contributing

See the `CONTRIBUTING.md` file.

## License

The license of this repository is as follows:

* Documentation text is under `CC-BY-4.0` license
* Scripts, tools, and so on, are under `Apache-2.0` license

See the `LICENSES` subdirectory for full license texts.
