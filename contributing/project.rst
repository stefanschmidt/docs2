.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. This is a boilerplate to be used by the CONTRIBUTING.sh tool. Do not include
   it as part of building the documentation.

``%PROJECTNAME%``-specific contributions process and guidelines
###############################################################

%POPULATEME%
