.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

REUSE Compliance
################

.. contents::
   :depth: 3

SPDX Information and REUSE Standard
***********************************

All projects and files for an hosted project **MUST** be `REUSE <https://reuse.software/>`_
compliant. REUSE requires SPDX information for each file, rules for which are
as follows:

* Any new file must have a SPDX header (copyright and license).
* For files that don't support headers (for example binaries, patches etc.) an associated ``.license`` file must be included with the relevant SPDX information.
* Do not add Copyright Year as part of the SPDX header information.
* The general rule of thumb for the license of a patch file is to use the license of the component for which the patch applies.
* When modifying a file through this contribution process, you may (but don't have to) claim copyright by adding a copyright line.
* Never alter copyright statements made by others, but only add your own.

Some files will make an exception to the above rules as described below:

* Files for which copyright is not claimed and for which this information was not trivial to fetch (for example backporting patches, importing build recipes etc. when upstream doesn't provide the SPDX information in the first place)
* license files (for example ``common-licenses`` in bitbake layers)

SPDX Header Example
-------------------

Make sure all of your submitted new files have a licensing statement in the headers. Please make sure that the license for your file is consistent with the licensing choice at project level and that you select the correct SPDX identifier, as in the following example for Apache 2.0 license:

.. code-block:: text

    /*
     * SPDX-FileCopyrightText: Jane Doe <jane@example.com>
     *
     * SPDX-License-Identifier: Apache-2.0
     */

Substantial Contributions
-------------------------

Therefore, if your contribution is only a patch directly applied to an existing file, then you are not required to do anything. If your contribution is an entire new project, or a substantial, copyrighted contribution, you **MUST** make sure that you do that following the `IP Policy <https://git.ostc-eu.org/oss-compliance/ip-policy/>`_ and that you comply with REUSE standard to include the licensing information where they are required.
